<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Registration</title>
	<style>
			div{
				margin-top: 5px;
				margin-bottom: 5px;
			}
		</style>
	</head>
<body>
		<div id ="container">
		
			<h1>Welcome to Servlet Job Finder!</h1>
		
			<form action="register" method="post">
				<!-- First Name -->
				<div>
					<label for="firstname">First Name</label>
					<input type="text" name="firstname" required>
				</div>
				
				<!-- Last Name -->
				<div>
					<label for="lastname">Last Name</label>
					<input type="text" name="lastname" required>
				</div>
				
				<!-- Phone Number -->
				<div>
					<label for="phone">Phone</label>
					<input type="tel" name="phone" required>
				</div>
				
				<!-- Email Address -->
				<div>
					<label for="email">Email</label>
					<input type="email" name="email" required>
				</div>
				
				<!-- App Discovery Choice -->
				<fieldset>
					<legend>How did you discover the app?</legend>
					<!-- Friends -->
					<table>
					<tr>
					<td><input type="radio" id="friends" name="discovery_type" value="friends" required></td>
					<td><label for="friends">Friends</label></td>
					</tr>
					<tr>
					<!-- Social Media -->
					<td><input type="radio" id="social_media" name="discovery_type" value="Social Media" required></td>
					<td><label for="social_media">Social Media</label></td>
					</tr>
					<!-- Others -->
					<tr>
					<td><input type="radio" id="others" name="discovery_type" value="Others" required></td>
					<td><label for="others">Others</label></td>
					</tr>
					</table>
				</fieldset>
				
				<!-- Date of Birth -->
				<div>
					<label for="date_of_birth">Date of birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
				<!-- User Type -->
				<div>
					<label for="userType">Are you an employer or applicant?</label>
					<select id="user_type" name="userType">
						<option value="" selected="selected">Select One</option>
						<option value="Applicant">Applicant</option>
						<option value="Employer">Employer</option>
					</select>
				</div>
				
				<!-- Profile Description -->
				<div>
					<label for="description">Profile Description</label>
					<textarea name="description" maxlength="500"></textarea>
				</div>
				
				<!-- Button for Submit -->
				<button>Register</button>
			</form>
		</div>
</body>
</html>