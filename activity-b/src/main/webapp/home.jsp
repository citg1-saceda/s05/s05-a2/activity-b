<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home</title>
	</head>
	<body>
		<% String fullname = session.getAttribute("fullname").toString(); %>
		<% String usertype = session.getAttribute("usertype").toString(); %>
		
		<h1>Welcome <%= fullname %>!</h1>
		
		<% 
			if(usertype.equals("Applicant")){
				out.println("<p>Welcome " + usertype + ". You may now start looking for your career opportunity.</p>");
			}else{
				out.println("<p>Welcome " + usertype + ". You may now start browsing applicant profiles.</p>");
			}
		
		
		%>
	</body>
</html>